package onlineshopping.readybytes.net.testmyapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class InstallReferrerReciever extends BroadcastReceiver {
    SharedPreferences prefs, appSharedPrefs;
    SharedPreferences.Editor prefsEditor;
    public static final String KEY_UTM_SOURCE = "utm_source";
    public static final String KEY_UTM_CONTENT = "utm_medium";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Map<String,String> Utm_Map = new HashMap<String,String >();
        String referrer = intent.getStringExtra("referrer");
        //https://play.google.com/store/apps/details?id=onlineshopping.readybytes.net.testmyapp&referrer=utm_source%3Drbsl%26utm_medium%3Demail%26anid%3Dadmob
        try {
            referrer= Uri.decode(URLDecoder.decode(referrer,"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (referrer != null && !referrer.equals("")) {
            String[] referrerParts = referrer.split("&");
            for (int i = 0; i <referrerParts.length ; i++) {
               String utm_List =  referrerParts[i];
                String[] utm_source = utm_List.split("=");
                if(utm_source.length==2) {
                    Utm_Map.put(utm_source[0], utm_source[1]);
                }
                }
            Log.d("UTM_MAP",Utm_Map.toString());
           if(Utm_Map.get("referrerId")!=null){
            if(context!=null){
              Intent intent1  = new Intent(context,BaseActivity.class);
                intent1.putExtra("referrerId",Utm_Map.get("referrerId").toString());
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               context.startActivity(intent1);
           }
        }
        }
}


    }

