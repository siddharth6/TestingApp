package onlineshopping.readybytes.net.testmyapp;

import android.app.*;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        if(findViewById(R.id.continuebutton)!=null) {
            findViewById(R.id.continuebutton).setOnClickListener(this);
        }

        TextView referrerId = (TextView)findViewById(R.id.referrerId);
            referrerId.setText(getIntent().getStringExtra("referrerId"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.invite_button:
                Intent intent = new Intent(this, LauncherActivity.class);
                getApplicationContext().startActivity(intent);
                break;
        }
    }
}
